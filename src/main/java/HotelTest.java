
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class HotelTest {
    Hotel obj = new Hotel();

    @Test
    public void CustDetailsTestLuxuryDoubleRoom(){
        obj.CustDetails(1,9);
        String expectedName = "sevina";
        String expectedContact = "1234567890";
        String expectedGender = "female";
        String expectedName2 = "sevina";
        String expectedContact2 = "1234567890";
        String expectedGender2 = "female";

        assertEquals(expectedName,obj.hotel_ob.luxury_doublerrom[9].name);
        assertEquals(expectedContact,obj.hotel_ob.luxury_doublerrom[9].contact);
        assertEquals(expectedGender,obj.hotel_ob.luxury_doublerrom[9].gender);

        assertEquals(expectedName2,obj.hotel_ob.luxury_doublerrom[9].name);
        assertEquals(expectedContact2,obj.hotel_ob.luxury_doublerrom[9].contact);
        assertEquals(expectedGender2,obj.hotel_ob.luxury_doublerrom[9].gender);
    }

    @Test
     public void CustDetailsTestLuxurySingleRoom(){
        obj.CustDetails(3,9);
        String expectedName = "sevina";
        String expectedContact = "1234567890";
        String expectedGender = "female";
        assertEquals(expectedName,obj.hotel_ob.luxury_singleerrom[9].name);
        assertEquals(expectedContact,obj.hotel_ob.luxury_singleerrom[9].contact);
        assertEquals(expectedGender,obj.hotel_ob.luxury_singleerrom[9].gender);
    }


    @Test
    public void checkFeaturesForRoom(){
        assertEquals("Number of double beds : 1\nAC : Yes\nFree breakfast : Yes\nCharge per day:4000 ", obj.features(1));
        assertEquals("Number of double beds : 1\nAC : No\nFree breakfast : Yes\nCharge per day:3000  ", obj.features(2));
        assertEquals("Number of single beds : 1\nAC : Yes\nFree breakfast : Yes\nCharge per day:2200  ", obj.features(3));
        assertEquals("Number of single beds : 1\nAC : No\nFree breakfast : Yes\nCharge per day:1200 ", obj.features(4));
    }



    @Test
    public void Checkavailability(){
//     obj.availability(1);
     assertEquals("Number of rooms available : "+10, obj.availability(1));
     assertEquals("Number of rooms available : "+20, obj.availability(2));
     assertEquals("Number of rooms available : "+10, obj.availability(3));
     assertEquals("Number of rooms available : "+20, obj.availability(4));
    }

    @Test
    public void NullPointerException(){
        obj.CustDetails(3,9);
        assertThrows(NullPointerException.class,()->{
            obj = null;
        });
    }

}