import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class FoodTest {

    Food obj = new Food(2,1);
    @Test
    public void checkFoodPrice(){
        Food obj = new Food(2,1);
        float priceOfFood = obj.price;
        float expectedResult = 60.00F;
        assertTrue(60.00 == obj.price);
//        assertEquals(expectedResult,priceOfFood);
    }
    @Test
    public void checkWrongInput(){
        Food obj = new Food(5,1);
        float expectedResult = 0F;
        assertTrue(expectedResult == obj.price);
    }
    @Test
    public void NullPointerException(){
        assertThrows(NullPointerException.class,()->{
            obj = null;
        });
    }

}