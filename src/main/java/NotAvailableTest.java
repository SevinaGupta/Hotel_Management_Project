
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;


public class NotAvailableTest {

    NotAvailable obj=new NotAvailable();

//    @Before
//    public void Sample(){
//        Assert.assertEquals(1,1);
//    }

    @Test
    public void NotAvailableTest(){
        assertEquals(1,1);
    }

    @Test
    public void RoomAvailable(){
        assertEquals("Not Available !",obj.toString());
    }

    @Test
    public void NullPointerException() {
        assertThrows("Throws Exception",NullPointerException.class, () -> {
            obj = null;
//            System.out.println(obj.toString());
        });
      }
    }


