import org.junit.Test;

import static org.junit.Assert.*;


public class SingleroomTest {

    Singleroom obj = new Singleroom("sevina", "7052024727", "female");
    @Test
    public void checkSingleRoomTest(){

        Singleroom obj = new Singleroom("sevina", "7052024727", "female");
        assertFalse(obj.name.isEmpty() && obj.contact.isEmpty() && obj.gender.isEmpty());
    }
    @Test
    public void checkNoValue(){
        Singleroom obj1 = new Singleroom();
        assertTrue(obj1.name.isEmpty());
    }
    @Test
    public void NullPointerException(){
        assertThrows("Throws Exception",NullPointerException.class,()->{
            obj = null;
        });
    }

}