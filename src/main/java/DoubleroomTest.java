import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.*;

public class DoubleroomTest {

    Doubleroom obj = new Doubleroom("sfas","132435","qwer","asddsf","123345","asfsd");

    @Test
    public void checkNoValue(){
        Doubleroom obj1 = new Doubleroom();
        assertTrue(obj1.name.isEmpty());
    }
    @Test
    public void checkValueEmpty(){
        assertFalse(obj.name.isEmpty() && obj.contact.isEmpty() && obj.gender.isEmpty() && obj.name2.isEmpty() && obj.contact2.isEmpty() && obj.gender2.isEmpty());
    }
    @Test
    public void checkEqual(){
        assertEquals("sfas",obj.name);
    }
    @Test
    public void NullPointerException() {
       assertThrows(NullPointerException.class, () -> {
        obj = null;
    });
    }


}