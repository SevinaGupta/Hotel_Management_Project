import org.junit.Assert;
import org.junit.Test;

public class holderTest {
    holder obj = new holder();
    @Test
    public void checkAvailabilitySingleLuxuryRoom(){
        int expectedResult = 10;
        Assert.assertEquals(expectedResult,obj.luxury_singleerrom.length);
    }
    @Test
    public void checkAvailabilitySingleDeluxeRoom(){
        int expectedResult = 20;
        Assert.assertEquals(expectedResult,obj.deluxe_singleerrom.length);
    }
    @Test
    public void checkAvailabilityDoubleLuxuryRoom(){
        int expectedResult = 10;
        Assert.assertEquals(expectedResult,obj.luxury_doublerrom.length);
    }
    @Test
    public void checkAvailabilityDoubleDeluxeRoom(){
        int expectedResult = 20;
        Assert.assertEquals(expectedResult,obj.deluxe_doublerrom.length);
    }

}
